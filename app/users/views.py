from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from django.core import serializers

from app.users.models import User


class ListUsers(APIView):
    def get(self, request):

        users = serializers.serialize("json", User.objects.all())
        response = {
            'users': users
        }
        return Response(response, status=status.HTTP_200_OK)
