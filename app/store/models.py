from django.db import models


class Products(models.Model):
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    # categories = models.ManyToManyField(Category)


    def activate(self):
        self.is_active = True

    def deactivate(self):
        self.is_active = False
