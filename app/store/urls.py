from django.urls import path
from .views import ListProducts, CreateProducts

app_name = 'store'

urlpatterns = [
    path('products', ListProducts.as_view()),
    path('products/post', CreateProducts.as_view(), name="create")
]
